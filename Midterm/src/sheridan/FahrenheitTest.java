package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;



public class FahrenheitTest {

	@Test
	public void testIsValidReg() {
		int passCheck = Fahrenheit.fromCelsius(0);
		assertEquals(32, passCheck);
	}
	
	@Test
	public void testIsValidEx() {
		int passCheck = Fahrenheit.fromCelsius(5);
		assertNotEquals(32, passCheck);
	}	
	
	@Test
	public void testIsValidBIn() {
		int passCheck = Fahrenheit.fromCelsius(-1);
		assertEquals(30, passCheck);
	}
	
	@Test
	public void testIsValidBOut() {
		int passCheck = Fahrenheit.fromCelsius(1);
		assertNotEquals(33, passCheck);
	}
	//Author James Hongxun Li lihongxu


}
